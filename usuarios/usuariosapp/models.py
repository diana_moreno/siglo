from django.db import models

# Create your models here.
class User(models.Model):
    firstName = models.CharField("First name", max_length=255)
    lastName = models.CharField("Last name", max_length=255)
    email = models.EmailField()
    phone = models.CharField(max_length=20, null = True)
    address = models.TextField(blank=True, null=True)
    createdAt = models.DateTimeField("Created At", auto_now_add=True)
