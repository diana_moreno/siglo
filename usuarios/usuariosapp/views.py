from django.shortcuts import render, redirect, get_object_or_404
from .models import User
from .forms import UserForm
from django.views.generic import ListView

# Create your views here.
class IndexView(ListView):
    template_name = 'index.html'
    context_object_name = 'user_list'

    def get_queryset(self):
        return User.objects.all()
 

def create(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    form = UserForm()

    return render(request,'create.html',{'form': form})

def edit(request, pk, template_name='edit.html'):
    user = get_object_or_404(User, pk=pk)
    form = UserForm(request.POST or None, instance=user)
    if form.is_valid():
        form.save()
        return redirect('index')
    return render(request, template_name, {'form':form})

def delete(request, pk, template_name='delete.html'):
    user = get_object_or_404(User, pk=pk)
    if request.method=='POST':
        user.delete()
        return redirect('index')
    return render(request, template_name, {'object':user})